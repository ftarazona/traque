use chrono::{DateTime, Utc};

pub struct TimedPosition {
    date: DateTime<Utc>,
    position: u32,
}

pub struct Team {
    name: &'static str,
    last_position: TimedPosition,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
