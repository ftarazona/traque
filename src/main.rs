use chrono::{DateTime, Timelike, Utc};
use std::env;
use std::io;
use std::io::Write;

#[derive(Copy, Clone, Debug)]
pub struct TimedPosition {
    date: DateTime<Utc>,
    position: char,
}

impl TimedPosition {
    fn new() -> Self {
        TimedPosition {
            date: Utc::now(),
            position: '?',
        }
    }
    fn update(&mut self, position: char) {
        self.date = Utc::now();
        self.position = position;
    }
    fn position(&self) -> char {
        self.position
    }
    fn time(&self) -> String {
        format!("{:02}:{:02}", self.date.hour() + 2, self.date.minute())
    }
}

fn main() {
    let n_teams: Vec<String> = env::args().collect();
    let n_teams: usize = n_teams[1].trim().parse().unwrap();

    let mut pos_teams: Vec<TimedPosition> = vec![TimedPosition::new(); n_teams];
    let mut chain: Vec<usize> = (0..n_teams).map(|x| (x + 1) % n_teams).collect();
    let mut scores: Vec<i32> = vec![0; n_teams];


    loop {
        print!(">>> ");
        let mut line = String::new();
        io::stdout().flush().unwrap();
        io::stdin().read_line(&mut line).unwrap();
        let line = line.to_lowercase().replace("\n", "");
        let mut line = line.split(' ');
        let cmd = line.next();
        if let Some(cmd) = cmd {
            if cmd == "update" {
                let team = line.next();
                let pos = line.next();
                if let (Some(team), Some(pos)) = (team, pos) {
                    let team = team.trim().parse::<usize>();
                    let pos = pos.trim().parse::<char>();
                    if let (Ok(i_team), Ok(pos)) = (team, pos) {
                        let team = pos_teams.get_mut(i_team);
                        if let Some(team) = team {
                            team.update(pos.to_ascii_uppercase());
                            println!(
                                "Team {} updated to position {} at {}",
                                i_team,
                                team.position(),
                                team.time()
                            );
                            let prey = pos_teams[chain[i_team]];
                            println!(
                                "Prey {} was at position {} at {}",
                                chain[i_team],
                                prey.position(),
                                prey.time()
                            );
                        }
                    }
                }
            } else if cmd == "find" {
                let team = line.next();
                if let Some(team) = team {
                    let team = team.trim().parse::<usize>();
                    if let Ok(i_team) = team {
                        let team = pos_teams.get(i_team);
                        if let Some(team) = team {
                            let prey = pos_teams[chain[i_team]];
                            println!(
                                "Team {} was at position {} at {}\nPrey {} was at position {} at {}",
                                i_team,
                                team.position(),
                                team.time(),
                                chain[i_team],
                                prey.position(),
                                prey.time()
                            );
                        }
                    }
                }
            } else if cmd == "scores" {
                for i_team in 0..n_teams {
                    let team = pos_teams[i_team];
                        println!(
                            "Team {} was at position {} at {}, score : {}, preys team {}",
                            i_team,
                            team.position(),
                            team.time(),
                            scores[i_team],
                            chain[i_team]
                        );
                }
            } else if cmd == "point" {
                let team = line.next();
                if let Some(team) = team {
                    let team = team.trim().parse::<usize>();
                    if let Ok(i_team) = team {
                        scores[i_team] += 1;
                        scores[chain[i_team]] -= 1;
                        let t1 = i_team;
                        let t2 = chain[t1];
                        let t3 = chain[t2];
                        let t4 = chain[t3];
                        chain[t1] = t3;
                        println!("Team {} now preys team {} (position {} at {})", t1, t3, pos_teams[t3].position(), pos_teams[t3].time());
                        chain[t3] = t2;
                        println!("Team {} now preys team {} (position {} at {})", t3, t2, pos_teams[t2].position(), pos_teams[t2].time());
                        chain[t2] = t4;
                        println!("Team {} now preys team {} (position {} at {})", t2, t4, pos_teams[t4].position(), pos_teams[t4].time());
                    }
                }
            }
        }
    }
}
